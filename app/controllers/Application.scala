package controllers

import play.api._
import play.api.mvc._
import play.api.libs.ws.WS
import play.api.libs.json.Json
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.concurrent.atomic.AtomicLong

case class Saying(id:Long, msg:String)
object Application extends Controller {
  val lastId = new AtomicLong(0);
  val url = Play.configuration.getString("app.origin", None).getOrElse("http://localhost:8000")
  def index = Action.async {
    WS.url(url).get.map { r => 
      val p = r.body
      val msg = p + " " + p.reverse
      implicit val sayingJsonFormatter = Json.format[Saying]
      val result = Json.toJson(Saying(lastId.incrementAndGet(), msg))
      Ok(result)
    }
  }

  def hello = Action { Ok("Hello, world!") }
}
